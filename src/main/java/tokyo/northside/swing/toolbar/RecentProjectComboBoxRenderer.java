package tokyo.northside.swing.toolbar;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RecentProjectComboBoxRenderer implements ListCellRenderer<URI> {

    private static final int MAX_PATH_LEN = 50;

    public RecentProjectComboBoxRenderer() {
    }

    void setComponent(JPanel panel, Path path) {
        Border border = LineBorder.createBlackLineBorder();
        panel.setBorder(border);
        panel.setOpaque(false);
        panel.setLayout(new BorderLayout());
        JLabel projectName = new JLabel(getProjectName(path));
        projectName.setFont(panel.getFont().deriveFont(Font.BOLD, 16f));
        JLabel typeLabel = new JLabel("(Project Type)");
        JLabel projectPathLabel = new JLabel(getShortenPath(path));
        panel.add(projectName, BorderLayout.CENTER);
        panel.add(typeLabel, BorderLayout.NORTH);
        panel.add(projectPathLabel, BorderLayout.SOUTH);
    }

    private String getProjectName(Path path) {
        String filename = path.getFileName().toString();
        return filename.substring(0, filename.length() - 5);
    }

    private String getShortenPath(Path path) {
        Path home = Paths.get(System.getProperty("user.home"));
        String result;
        if (path.startsWith(home)) {
            result = "~/" + home.relativize(path);
        } else {
            result = path.toString();
        }
        if (result.length() > MAX_PATH_LEN) {
            result = result.substring(0, MAX_PATH_LEN) + "...";
        }
        return result;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends URI> list, URI value, int index, boolean isSelected, boolean cellHasFocus) {
        JPanel panel = new JPanel();
        if (index < 0) {
            panel.setLayout(new FlowLayout(FlowLayout.LEADING));
            JLabel label = new JLabel(getProjectName(Path.of(value)));
            panel.add(label);
        } else {
            if (value != null) {
                setComponent(panel, Path.of(value));
            }
        }
        return panel;
    }

}
