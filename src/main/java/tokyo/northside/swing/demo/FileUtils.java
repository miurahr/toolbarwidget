package tokyo.northside.swing.demo;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class FileUtils {

    /** Caching install dir */
    private static String installDir = null;

    /**
     * Returns demo app installation directory.
     */
    public static String installDir() {
        if (installDir == null) {
            File file = null;
            try {
                URI sourceUri = App.class.getProtectionDomain().getCodeSource().getLocation().toURI();
                if (sourceUri.getScheme().equals("file")) {
                    File uriFile = Paths.get(sourceUri).toFile();
                    // If running from a JAR, get the enclosing folder
                    // (the JAR is assumed to be at the installation root)
                    if (uriFile.getName().endsWith(".jar")) {
                        file = uriFile.getParentFile();
                    }
                }
            } catch (URISyntaxException ignored) {
            }
            if (file == null) {
                file = Paths.get(".").toFile();
            }
            installDir = file.getAbsolutePath();
        }
        return installDir;
    }
}
