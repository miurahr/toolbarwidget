/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2014 Briac Pilpre, Aaron Madlon-Kay
               2015 Aaron Madlon-Kay
               Home page: https://www.omegat.org/
               Support center: https://omegat.org/support

 This file is part of OmegaT.

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************/

package tokyo.northside.swing.demo;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

/**
 * Management of recent projects
 *
 * @author Briac Pilpre
 * @author Aaron Madlon-Kay
 */
public final class RecentProjects {

    private static final List<String> RECENT_PROJECTS;
    private static final int MOST_RECENT_PROJECT_SIZE = 10;

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        RECENT_PROJECTS = new ArrayList<>(MOST_RECENT_PROJECT_SIZE);
        try {
            Path jsonFile = Paths.get(FileUtils.installDir(), "recent.json");
            List<String> list = mapper.readValue(jsonFile.toFile(), FileList.class).getFiles();
            for (int i = 0; i < Integer.min(MOST_RECENT_PROJECT_SIZE, list.size()); i++) {
                String project = list.get(i);
                if (project != null) {
                    RECENT_PROJECTS.add(project);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private RecentProjects() {
    }

    public static List<String> getRecentProjects() {
        synchronized (RECENT_PROJECTS) {
            return new ArrayList<>(RECENT_PROJECTS);
        }
    }

    public static void add(String element) {
        if (StringUtils.isEmpty(element)) {
            return;
        }

        synchronized (RECENT_PROJECTS) {
            RECENT_PROJECTS.remove(element);
            RECENT_PROJECTS.add(0, element);

            // Shrink the list to match the desired size.
            while (RECENT_PROJECTS.size() > MOST_RECENT_PROJECT_SIZE) {
                RECENT_PROJECTS.remove(MOST_RECENT_PROJECT_SIZE);
            }
        }
    }

    public static void clear() {
        synchronized (RECENT_PROJECTS) {
            RECENT_PROJECTS.clear();
        }
    }

    static class FileList {
        private List<String> files;

        public List<String> getFiles() {
            return files;
        }

        public void setFiles(List<String> files) {
            this.files = files;
        }
    }
}
