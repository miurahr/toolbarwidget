package tokyo.northside.swing.demo;

import org.fit.cssbox.swingbox.BrowserPane;
import tokyo.northside.swing.toolbar.RecentProjectComboBoxRenderer;
import tokyo.northside.swing.toolbar.ToolbarComboboxUI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

@SuppressWarnings("unchecked")
public class App extends JPanel {
    private static final JFrame frame = new JFrame();

    private final JButton closeButton = new JButton("Close");


    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setTitle("\"Toolbar widget\" Application");
                App app = new App();
                frame.getContentPane().add(app);
                frame.getRootPane().setDefaultButton(app.closeButton);
                frame.setPreferredSize(new Dimension(950, 500));
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public App() throws IOException {
        init();
    }

    private void init() throws IOException {
        // Content
        URI uri = getDocsURI("start.html");
        if (uri == null) {
            return;
        }
        URL url = uri.toURL();
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setOpaque(false);
        scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        BrowserPane browser = new BrowserPane();
        browser.setEditable(false);
        browser.setPage(url);
        browser.setFont(getFont());
        scrollPane.getViewport().setView(browser);

        add(scrollPane, BorderLayout.CENTER);
        add(closeButton, BorderLayout.SOUTH);
        closeButton.addActionListener(e -> {
            if (frame.isDisplayable()) {
                frame.dispose();
            }
        });

        JPanel toolbar = new JPanel(new FlowLayout(FlowLayout.LEADING));
        JLabel selectorTitle = new JLabel();
        selectorTitle.setText("Select target:");
        toolbar.add(selectorTitle);
        JComboBox<URI> combobox = new JComboBox<>();
        RecentProjects.getRecentProjects().stream().map(App::getDocsURI).forEach(combobox::addItem);
        combobox.setRenderer(new RecentProjectComboBoxRenderer());
        combobox.setUI(new ToolbarComboboxUI());
        combobox.setPreferredSize(new Dimension(350, 30));
        toolbar.add(combobox);

        combobox.addActionListener(e -> {
            URI target = (URI) combobox.getSelectedItem();
            try {
                browser.setPage(target.toURL());
            } catch (IOException ignored) {
            }
        });

        add(toolbar, BorderLayout.NORTH);
    }

    // --- utilities --

    public static URI getDocsURI(String filename) {
        // find in install_dir
        File file = Paths.get(FileUtils.installDir(),"src", "docs", filename).toFile();
        if (file.isFile()) {
            return file.toURI();
        }
        // find in classpath
        URL url = App.class.getResource('/' + filename);
        if (url != null) {
            try {
                return url.toURI();
            } catch (URISyntaxException ex) {
                throw new RuntimeException(ex);
            }
        }
        return null;
    }
}
