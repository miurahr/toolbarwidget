
plugins {
    java
    application
    id("com.github.spotbugs") version "5.0.9"
    id("com.diffplug.spotless") version "6.7.2"
}

group = "tokyo.northside"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("tokyo.northside.swing.demo.App")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("tokyo.northside:swingbox:1.2.4")
    implementation("org.apache.commons:commons-lang3:3.6")
    implementation("com.fasterxml.jackson.core:jackson-core:2.14.2")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.14.2")
    runtimeOnly("org.slf4j:slf4j-simple:1.7.32")
}

spotbugs {
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()

    // Test in headless mode with ./gradlew test -Pheadless
    if (project.hasProperty("headless")) {
        systemProperty("java.awt.headless", "true")
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

sourceSets {
    java {
        main {
            resources {
                setSrcDirs(listOf("src/docs", "src/main/resources"))
            }
        }
    }
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.md", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        palantirJavaFormat()
        importOrder()
        removeUnusedImports()
    }
}

tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}